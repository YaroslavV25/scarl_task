import os
import logging
from time import localtime, strftime
from billing.config import SQLITE_DIR, OBJECT_TYPES
from billing.sqlite_handler import SQLiteHandler


class BillingDbHandler(SQLiteHandler):
    '''
    This class implements basic methods for fulfilling cost-calculation
    needs to data base usage.
    '''
    __instance = None

    def __new__(cls,
                **kwargs):
        if BillingDbHandler.__instance is None:
            BillingDbHandler.__instance = object.__new__(cls)
        return BillingDbHandler.__instance

    def __init__(self):
        '''
        Input:
        '''
        time_stamp = strftime('%d-%b-%Y.%H.%M.%S', localtime())
        self.db_path = os.path.join(SQLITE_DIR,
                                    '{0}.sqlite3'.format(time_stamp))
        logging.info('Data base path: {0}'.format(self.db_path))
        super().__init__(db_path=self.db_path)
        self.create_tables()

    def create_object_types_table(self):
        (self.cursor.
         execute('''
                 CREATE TABLE IF NOT EXISTS object_types(
                 id INTEGER PRIMARY KEY,
                 type TEXT
                 )
                 '''))
        self.db.commit()

    def insert_object_types(self):
        objects_sets = [(obj,) for obj in OBJECT_TYPES]
        if not self.select_from_table_vals(table_name='object_types',
                                           field_to_sel='type',
                                           field_to_comp='type',
                                           values_to_comp=OBJECT_TYPES):
            (self.cursor.
             executemany('''
                         INSERT INTO object_types ('type')
                         VALUES (?)
                         ''', objects_sets))
            self.db.commit()

    def set_types_enum(self):
        self.types_enum = {}
        for obj in OBJECT_TYPES:
            self.types_enum[obj] =\
                self.select_from_table(table_name='object_types',
                                       field_to_sel='id',
                                       field_to_comp='type',
                                       value_to_comp=obj)
        return self.types_enum

    def create_costs_table(self):
        (self.cursor.
         execute('''
                 CREATE TABLE IF NOT EXISTS costs_info (
                 id INTEGER PRIMARY KEY,
                 object_type integer NOT NULL,
                 object_id TEXT,
                 cost REAL,
                 FOREIGN KEY (object_type) REFERENCES object_types (id)
                 )
                 '''))
        self.db.commit()

    def create_tables(self):
        logging.info('Creating tables')
        self.create_object_types_table()
        self.insert_object_types()
        self.set_types_enum()
        self.create_costs_table()

    def ubdate_cost_by_type(self, object_type, object_val, cost):
        table_name = 'costs_info'
        type_pk = self.types_enum[object_type]
        cost_row_id = self.select_from_table(table_name=table_name,
                                             field_to_sel='id',
                                             field_to_comp='object_id',
                                             value_to_comp=object_val)

        if cost_row_id:
            self.update_field_add(table_name=table_name,
                                  field_to_upd='cost',
                                  val_to_add=cost,
                                  field_to_comp='id',
                                  value_to_comp=cost_row_id)
        else:
            self.insert_row(table_name=table_name,
                            fields_set=('object_type', 'object_id', 'cost'),
                            vals_set=(type_pk, object_val, cost))
