import logging
from concurrent.futures import ThreadPoolExecutor
from billing.logging_handler import LogAgent
from billing.billing_db_handler import BillingDbHandler
from billing.csv_handler import CsvHandler
from billing.config import MAX_THREADS


class CostHandler():
    '''
    This class is general in cost-calculation scripts. Triggers parallel
    execution of calculating per each packed csv url.
    '''
    def __init__(self, urls_list):
        self.urls_list = urls_list
        self.executor = ThreadPoolExecutor(max_workers=MAX_THREADS)
        self.db_handler = BillingDbHandler()
        self.logger = LogAgent()

    def calculate_costs(self):
        logging.info('Urls processing started.')
        for csv_url in self.urls_list:
            (self.executor.
             submit(CsvHandler(url_path=csv_url,
                               logger=self.logger,
                               db_handler=self.db_handler).execute(),))
        self.executor.shutdown(wait=True)
        self.db_handler.db.commit()
        self.db_handler.db.close()
        self.logger.log_final_results()
        logging.info('Execution results are stored in:\n  {0}'
                     .format(self.db_handler.db_path))
