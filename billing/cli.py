from argparse import ArgumentParser, RawDescriptionHelpFormatter
from datetime import datetime
import logging
import traceback
from billing.api import cost_calculation as cost_calculation_api
from billing.logging_handler import LogAgent


def cost_calculation():
    """
    This function describes cost-calculation command, its arguments, their
    descriptions, and properties
    """
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-urls', '--csv_urls',
                        nargs='+',
                        required=True,
                        dest='csv_urls',
                        default=[],
                        help='Specify csv files urs list for input. ')
    args = parser.parse_args()
    command_name = 'cost-calculation'
    start_time = datetime.now()
    result = 0
    try:
        LogAgent()  # initialize LogAgent to setup logging
        cost_calculation_api(csv_urls=args.csv_urls)
    except BaseException as e:
        logging.debug(traceback.format_exc())
        logging.error(str(e))
        result = 100
    finally:
        end_time = datetime.now()
        logging.info('Duration time: {}'.format(end_time - start_time))
        if result == 0:
            logging.debug('{} finished correctly.'.format(command_name))
        else:
            logging.error('{} failed.'.format(command_name))
        return result
