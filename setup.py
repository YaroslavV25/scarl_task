from distutils.core import setup
from setuptools import find_packages
from billing import __version__

description =\
    '''
    Script calculating total cost per object_type, object_id
    for all rows from csv files that have user:scalr-meta field by type
    v1:{SCALR_ENV_ID}:{SCALR_FARM_ID}:{SCALR_FARM_ROLE_ID}:{SCALR_SERVER_ID}
    with not-empty values.
    Where:
        object_type - one of four resource types: env, farm, farm_role, serve
        object_id - its value in scarl-meta tag
    The results are stored in sqlite table.
    '''

setup(name='scarl_task',
      version=__version__,
      packages=find_packages(),
      url='',
      license='',
      author='yaroslav',
      author_email='varkhol.y@gmail.com',
      description=description,
      install_requires=['flake8'],
      entry_points={'console_scripts':
                    ['cost-calculation = billing.cli:cost_calculation'],
                    },
      )
