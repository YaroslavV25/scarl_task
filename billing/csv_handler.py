import re
import traceback
from zipfile import ZipFile
from urllib.request import urlretrieve
from csv import DictReader


class CsvHandler():
    '''
    This class is dedicated to calculating total cost for each object_type
    per object_id from one url to packed csv file(s). The results are
    stored in self.db_handler data base.
    '''

    def __init__(self, url_path, logger, db_handler):
        self.url_path = url_path
        self.logger = logger
        self.db_handler = db_handler
        self.env_costs = {}
        self.farm_costs = {}
        self.farm_role_costs = {}
        self.server_costs = {}

    def read_csv_files(self):
        """
        This method reds csv files fro zip archive stored by url.
        """
        self.zip, _ = urlretrieve(self.url_path)
        with ZipFile(self.zip) as zf:
            csvfiles = [name for name in zf.namelist()
                        if name.endswith('.csv')]
            for filename in csvfiles:
                with zf.open(filename) as csvfile:
                    self.csv_reader = DictReader([line.decode('iso-8859-1')
                                                  for line in csvfile])
                    self.process_csv_rows()

    def save_in_costs_dict(self, costs_dict, object_id, cost):
        """
        This method updates separate costs dict with object_id as a key
        and cost as value. If object_id is already in costs_dict, the cost
        value is incremented.
        """
        if object_id not in costs_dict.keys():
            costs_dict[object_id] = float()
        costs_dict[object_id] += cost

    def process_csv_rows(self):
        '''
        This method parses user:scalr-meta info from each csv row and stores
        information in appropriate costs dict.
        '''
        for row in self.csv_reader:
            if row['user:scalr-meta']:
                pattern = 'v1:(.+):(.+):(.+):(.+)'
                m = re.search(pattern, row['user:scalr-meta'])
                if m:
                    vals_set = m.groups()  # (env, farm, farm_role, server)
                    self.save_in_costs_dict(costs_dict=self.env_costs,
                                            object_id=vals_set[0],
                                            cost=float(row['Cost']))
                    self.save_in_costs_dict(costs_dict=self.farm_costs,
                                            object_id=vals_set[1],
                                            cost=float(row['Cost']))
                    self.save_in_costs_dict(costs_dict=self.farm_role_costs,
                                            object_id=vals_set[2],
                                            cost=float(row['Cost']))
                    self.save_in_costs_dict(costs_dict=self.server_costs,
                                            object_id=vals_set[3],
                                            cost=float(row['Cost']))

    def update_costs_table(self):
        """
        This method updates costs_info table in self.db_handler data base
        with information taken from each costs dictionary.
        """
        for env_id, cost_val in self.env_costs.items():
            self.db_handler.ubdate_cost_by_type(object_type='env',
                                                object_val=env_id,
                                                cost=cost_val)
        for farm_id, cost_val in self.farm_costs.items():
            self.db_handler.ubdate_cost_by_type(object_type='farm',
                                                object_val=farm_id,
                                                cost=cost_val)
        for farm_role_id, cost_val in self.farm_role_costs.items():
            self.db_handler.ubdate_cost_by_type(object_type='farm_role',
                                                object_val=farm_role_id,
                                                cost=cost_val)
        for server_id, cost_val in self.server_costs.items():
            self.db_handler.ubdate_cost_by_type(object_type='server',
                                                object_val=server_id,
                                                cost=cost_val)

    def execute(self):
        '''
        This method is a wrapper for total functionality execution.
        '''
        try:
            self.read_csv_files()
            self.update_costs_table()
            self.logger.successful_files.append(self.url_path)
        except BaseException:
            error = traceback.format_exc()
            self.logger.add_failed_el(el_path=self.url_path,
                                      error=error)
