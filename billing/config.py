from os import path

BASE_DIR = path.dirname(path.abspath(__file__))
SQLITE_DIR = path.join(BASE_DIR, 'results_db')
OBJECT_TYPES = ['env', 'farm', 'farm_role', 'server']
MAX_THREADS = 100
