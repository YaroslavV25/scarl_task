import sqlite3


class SQLiteHandler():
    '''
    This class is dedicated to interactions with SQLite data base.
    '''

    def __init__(self, db_path):
        self.db_path = db_path
        self.db = sqlite3.connect(self.db_path)
        self.cursor = self.db.cursor()

    def read_table(self, table_name):
        self.cursor.execute("SELECT * FROM {0}".format(table_name))
        return self.cursor.fetchall()

    def select_from_table(self, table_name, field_to_sel, field_to_comp,
                          value_to_comp):
        self.cursor.execute('''
                            SELECT {0}
                            FROM {1}
                            where {2} = ?'''
                            .format(field_to_sel, table_name, field_to_comp),
                            (value_to_comp,))
        field_val = None
        res_set = self.cursor.fetchone()
        if res_set:
            field_val = res_set[0]
        return field_val

    def select_from_table_vals(self, table_name, field_to_sel, field_to_comp,
                               values_to_comp):
        self.cursor.execute('''
                            SELECT {0}
                            FROM {1}
                            where {2} in ({3})
                            '''
                            .format(field_to_sel, table_name, field_to_comp,
                                    ', '.join('?' for _ in values_to_comp)),
                            values_to_comp)
        return self.cursor.fetchall()

    def insert_row(self, table_name, fields_set, vals_set):
        (self.cursor.
         execute('''
                 INSERT INTO {0}({1})
                 VALUES({2})
                 '''
                 .format(table_name, ','.join(fields_set),
                         ', '.join('?' for _ in vals_set)), vals_set))

    def update_field(self, table_name, field_to_upd, val_for_upd,
                     field_to_comp, value_to_comp):
        self.cursor.execute('''
                            UPDATE {0}
                            SET {1} = {2}
                            where {3} = ?
                            '''
                            .format(table_name, field_to_upd, val_for_upd,
                                    field_to_comp),
                            (value_to_comp,))

    def update_field_add(self, table_name, field_to_upd, val_to_add,
                         field_to_comp, value_to_comp):
        self.cursor.execute('''
                            UPDATE {0}
                            SET {1} = {1} + {2}
                            where {3} = ?
                            '''
                            .format(table_name, field_to_upd, val_to_add,
                                    field_to_comp), (value_to_comp,))
