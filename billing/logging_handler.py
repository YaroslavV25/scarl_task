import logging
from collections import OrderedDict


class LogAgent():
    '''
    '''
    __instance = None

    def __new__(cls,
                **kwargs):
        if LogAgent.__instance is None:
            LogAgent.__instance = object.__new__(cls)
        return LogAgent.__instance

    def __init__(self):
        '''
        '''
        self.successful_files = []
        self.failed_els = {}
        self.setup_logging()

    @staticmethod
    def setup_logging():
        log = logging.getLogger()
        log.setLevel(logging.DEBUG)

    def add_failed_el(self, el_path, error):
        '''
        '''
        self.failed_els[el_path] = error

    def sort_dict(self, dictionary):
        '''
        '''
        dictionary = OrderedDict(sorted(dictionary.items()))
        return dictionary

    def header_to_print(self, header):
        '''
        '''
        return '\n  {0}:    \n'.format(header)

    def path_to_print(self, path):
        '''
        '''
        return '    {0}\n'.format(path)

    def error_to_print(self, error):
        '''
        '''
        return '      {0}\n'.format(error)

    def form_log_messeges(self):
        '''
        '''
        self.success_msg = ''
        self.warn_msg = ''
        self.error_msg = ''
        if self.successful_files:
            header = 'Successfully handled elements'
            self.success_msg = self.header_to_print(header)
            for file_path in sorted(self.successful_files):
                self.success_msg += self.path_to_print(path=file_path)
        if self.failed_els:
            self.failed_els = self.sort_dict(dictionary=self.failed_els)
            header = 'Failed elements'
            self.error_msg = self.header_to_print(header)
            for el_path, error in self.failed_els.items():
                self.error_msg += self.path_to_print(path=el_path)
                self.error_msg += self.error_to_print(error=error)

    def log_final_results(self):
        '''
        '''
        self.form_log_messeges()
        if self.success_msg:
            logging.info(self.success_msg)
        if self.error_msg or self.warn_msg:
            description = self.warn_msg + '\n' + self.error_msg
            raise Exception(description)
