import logging
from billing.logging_handler import LogAgent
from billing.cost_handler import CostHandler


def cost_calculation(csv_urls):
    '''
    :return:
    '''
    LogAgent()  # initialize LogAgent to setup logging
    logging.info('Costs calculation started.')
    handler_inst = CostHandler(urls_list=csv_urls)
    handler_inst.calculate_costs()
